﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GunsAbstract : MonoBehaviour {
    #region Params
    public GameObject gunPref,bulletPref;  //GunPrfabs
	private int numOfBullets,//Кол-во пуль,
	clipVolume,//Объем магазина
	gundmg;//Урон
    #endregion

    void Start () {
		gundmg = clipVolume = numOfBullets = 0;


	}
	
	void Update () {
		
	}
    public IEnumerator Reload(float resetTime)//Перезарядка
    {
        yield return new WaitForSeconds(resetTime);
        numOfBullets = clipVolume;

    }
    public void Shooting(float sp, int dmg, Transform strtpos, RaycastHit hit, int bulNum) // Метод стрельбы
    {
        clipVolume = bulNum;
        numOfBullets = bulNum;
        gundmg = dmg;

        if (numOfBullets != 0)
        {
            GameObject temp = Instantiate(bulletPref, strtpos.position, Quaternion.identity) as GameObject;
            temp.transform.position = Vector3.MoveTowards(this.transform.position, hit.point, sp);
            numOfBullets--;
        }
        else
        {
            StartCoroutine("Rreload");
        }
   }

}
