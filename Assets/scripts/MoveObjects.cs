﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class MoveObjects : MonoBehaviour {


	public int hp = 100;
	public float speed;
	public int damage;



	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void TakeDamage (int dmg) {
		this.hp -= dmg;
	}
		
}
