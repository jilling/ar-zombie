﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Manager : MonoBehaviour {

	//public GameObject hostage;
	//public GameObject hostage2;
	public List<GameObject> listHostages = new List<GameObject> ();

	public static Manager instnce;

	public GameObject player, zombie;
	public Transform target, target2, zombieSpawn, zombieSpawn2;
	private Manager () {}

	// Use this for initialization
	void Start () {
		instnce = this;
		//listHostages.Add(hostage);
		//listHostages.Add(hostage2);

	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void CreateObjects () {
		GameObject temp = Instantiate(player, target.position, Quaternion.identity) as GameObject;
		Manager.instnce.listHostages.Add (temp);
		GameObject temp2 = Instantiate(player, target2.position, Quaternion.identity) as GameObject;
		Manager.instnce.listHostages.Add (temp2);
		// zombie
		GameObject tempZ = Instantiate(zombie, zombieSpawn.position, Quaternion.identity) as GameObject;
		GameObject tempZ2 = Instantiate(zombie, zombieSpawn2.position, Quaternion.identity) as GameObject;
	}
}
