﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MoveObjects {

	public float speed;
	public int damage;
	public GameObject nearestHostage; // ближайший hostage
	public int StartTime = 0; // стартовое время когда зашли в область hostage
	public bool nearHostage; // находимся в области hostage

	// Use this for initialization
	void Start () {
		

		speed = 0.001f;
		damage = 10;

		// определяем ближайшего hostage и идем к нему
		float distnceForNearestHostage = 0f;

		foreach (GameObject item in Manager.instnce.listHostages) {

			if (distnceForNearestHostage == 0) { // если первый запуск
				nearestHostage = item;
				distnceForNearestHostage = Vector3.Distance(item.transform.position, transform.position);
			} else {
				float dist = Vector3.Distance(item.transform.position, transform.position);

				if (dist < distnceForNearestHostage) {
					nearestHostage = item;
					distnceForNearestHostage = dist;
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
		Move ();
	}

	void OnTriggerStay (Collider col) {
		if (col.gameObject.tag == "hostage") { // если подошли к hostage

			if (!nearHostage) {
				StartTime = (int)Time.time;
				nearHostage = true;
			}

			if ((Time.time - StartTime) >= 1) {
				nearestHostage.GetComponent<Hostage> ().TakeDamage (damage);
				StartTime = 0;
				nearHostage = false;
			}

		}
	}

	// двигаем врага к hostage
	void Move() {
		this.transform.position = Vector3.MoveTowards (this.transform.position, nearestHostage.transform.position, speed);
	}


}
